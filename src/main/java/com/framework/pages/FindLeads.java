package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import com.framework.design.ProjectMethods;

public class FindLeads extends ProjectMethods {

	@FindBy(how= How.NAME,using="firstName") WebElement eleFName;
	@FindBy(how= How.CLASS_NAME,using="x-btn-text") WebElement eleFindLead;
	
	public FindLeads enterFName() {
		clearAndType(eleFName, "Aswin");
		return this;
	}
	public ViewLead clickFindLead() {
		click(eleFindLead);
		return new ViewLead();
		
	}
}

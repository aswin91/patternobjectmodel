package com.framework.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyHomePage extends ProjectMethods {
	
	public MyHomePage() {
		 PageFactory.initElements(driver, this);      

		}
	@FindBy(how = How.CLASS_NAME,using="x-panel-header") WebElement eleclickLeads;
	public MyLeads clickLeads() {
		click(eleclickLeads);
		return new MyLeads();

		
	
	}

}

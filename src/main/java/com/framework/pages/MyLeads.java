package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.ClickAction;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class MyLeads extends ProjectMethods {
	
	public MyLeads() {
		 PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.LINK_TEXT,using="Create Lead") WebElement eleCreateLead;
	@FindBy(how = How.LINK_TEXT,using="Find Leads") WebElement eleFindLeads;
	public CreateLeads enterLeads() {
		click(eleCreateLead);
		return new CreateLeads();
	}
	public FindLeads clickFindLead() {
		click(eleFindLeads);
		return new FindLeads();
		
	}

}

package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeads extends ProjectMethods{
	
	public CreateLeads() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(how = How.ID, using = "createLeadForm_firstName") WebElement eleFirstName;
	@FindBy(how = How.ID, using = "createLeadForm_lastName") WebElement eleLastName;
	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCompanyName;
	@FindBy(how = How.CLASS_NAME, using = "smallSubmit") WebElement eleCreateLead;
	public void enterFirst() {
		
	}
	public CreateLeads enterFirstName(String firstName) {
		clearAndType(eleFirstName, firstName);
		return this;
	}
	public CreateLeads enterLastName(String lastName) {
		clearAndType(eleLastName, lastName);
		return this;
		
	}
	public CreateLeads entercompanyName(String companyName) {
		clearAndType(eleCompanyName, companyName);
		return this;
			
	}
	public ViewLead clickCLead() {
		click(eleCreateLead);
		return new ViewLead();
		
	}
	public void clickCreateLead() {
		
	}
}

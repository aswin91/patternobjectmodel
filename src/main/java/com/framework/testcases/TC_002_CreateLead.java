package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.CreateLeads;
import com.framework.pages.HomePage;
import com.framework.pages.LoginPage;
import com.framework.pages.MyHomePage;
import com.framework.pages.MyLeads;

public class TC_002_CreateLead extends ProjectMethods {
	
	@BeforeTest
	public void setData() {
		testCaseName="TC001_CreateLead";
		testDescription="Create Leads";
		author="Aswin";
		category="Smoke";
		testNodes="Leads";
		dataSheetName="TC002";
	}
	@Test(dataProvider="fetchData")
	public void createLead(String username, String password, String firstName, String lastName, String companyName) {
		new LoginPage()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		//new HomePage()
		.clickLink()
		//new MyHomePage()
		.clickLeads()
		//new MyLeads();
		.enterLeads()
		//new CreateLeads()
		.enterFirstName(firstName)
		.enterLastName(lastName)
		.entercompanyName(companyName)
		.clickCLead();
	}
	

}
